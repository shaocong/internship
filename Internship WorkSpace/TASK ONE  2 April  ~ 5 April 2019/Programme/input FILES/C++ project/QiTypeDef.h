//----------------------------------------------------------------------------
//
// Copyright (c) Quantum Inventions Private Limited (QI).
// All rights reserved.
//
// No part of this source code may be reproduced, stored in a retrieval system
// or transmitted in any form or by any means without the express written
// consent of QI.
//
// This file contains the type definitions for basic data types for various architectures.
//
#ifndef INC_QITYPEDEF_H
#define INC_QITYPEDEF_H

///////////////////////////////////     BASE TYPES  ////////////////////////////////////////////////////////////
//Type Definitions for 64bit compiler.
#ifdef  __LP64__

typedef     long                        qi_int64;
typedef     unsigned long               qi_uint64;

typedef     int                         qi_int32;
typedef     unsigned int                qi_uint32;

typedef     char                        qi_int8;
typedef     unsigned char               qi_uint8;

typedef     float                       qi_float;

typedef     double                      qi_double;

typedef     char                        qi_char;
typedef     unsigned char               qi_uchar;
typedef     wchar_t                     qi_wchar;

typedef     long                        qi_long;
typedef     unsigned long               qi_ulong;

typedef     bool                        qi_bool;

typedef     qi_uint8                    qi_bits8;

typedef     qi_uint32                   qi_bits32;

typedef     qi_uint64                   qi_bits64;

typedef     void*                       qi_void_p;

#else

typedef     long long                   qi_int64;
typedef     unsigned long long          qi_uint64;

typedef     int                         qi_int32;
typedef     unsigned int                qi_uint32;

typedef     char                        qi_int8;
typedef     unsigned char               qi_uint8;

typedef     float                       qi_float;

typedef     double                      qi_double;

typedef     char                        qi_char;
typedef     unsigned char               qi_uchar;
typedef     wchar_t                     qi_wchar;

typedef     long                        qi_long;
typedef     unsigned long               qi_ulong;

typedef     bool                        qi_bool;

typedef     qi_uint8                    qi_bits8;

typedef     qi_uint32                   qi_bits32;

typedef     qi_uint64                   qi_bits64;

typedef     void*                       qi_void_p;

#endif

#define LOCAL        static        /* Local symbol definition */
#define EXPORT                /* Global symbol definition */
#define IMPORT        extern        /* Global symbol reference */

#endif