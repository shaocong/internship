//----------------------------------------------------------------------------
//
// Copyright (c) Quantum Inventions Private Limited (QI).
// All rights reserved.
//
// No part of this source code may be reproduced, stored in a retrieval system
// or transmitted in any form or by any means without the express written
// consent of QI.
//
//-----------------------------------------------------------------------------
//  Edit History
//
//  $Log: <QSplash.cpp>,v $
//  
//-----------------------------------------------------------------------------//

#include "stdafx.h"
#include "QSplash.hpp"
#include "QResource.hpp"

#include "cdg_message_queue.hpp"

#include <fstream>

char *QID_MAP_LOGO_PATH = "Map-Logo-480x272.png";

/////////////////////////////////////////
/////For downloading data from web
//Internal
//CString g_sServerIP = _T("192.168.1.155");
//External
//CString g_sServerIP = _T("202.128.195.246");
/////////////////////////////////////////

#if USE_UNIFIED_COST
extern vector<int> gf_Weightage;
#endif

eLicense_Mode g_eLicense_Mode=eLICENSE_DEVICE;

#define STARTUP_PROFILING 0

/**----------------------------------------------------------------------------
* <strong> PURPOSE/PROCESS:</strong> This function is used to hide warning screen when timed out
*
* @param [in] void
* @return void
*
* <strong> AUTHOR:</strong> Aung
* 
* <strong> COMMENTS/HISTORY: </strong>
* 
* <strong> Notes from code: </strong>
------------------------------------------------------------------------------ */
void CQSplash::HideWarningScreen()
{
	CQComponent *pWarningScreen = this->GetComponentByID(QID_WARNING_SCREEN);

	if(pWarningScreen != NULL)
		pWarningScreen->SetVisible(FALSE);

	HandleStartupLogos();

    SwitchToNextScreen();

	RedrawWindow();
}

/**----------------------------------------------------------------------------
* <strong> PURPOSE/PROCESS:</strong> This function is main loading function (loading configs,images,fonts, POIs etc...) during startup screen
*
* @param [in] pParam LPVOID Parameter to be passed to the controlling function  
* @return DWORD  Always return 0 in current implementation
*
* <strong> AUTHOR:</strong> Srilekha,Linh,Sijie,Xiaoke,Tarun,Sengtat,Aung
* 
* <strong> COMMENTS/HISTORY: </strong> Added Error Handling for Opeing User Database
* 
* <strong> Notes from code: </strong>
------------------------------------------------------------------------------ */
DWORD CQSplash::LoadResourceThread(LPVOID pParam)
{
#if WAIT_FOR_SMOOTH_GPS_STARTUP
	Sleep(1000); //to resolve the hang issue during startup 
#endif

    CQSplash* pSplashScreen = (CQSplash*) pParam;
    if (pSplashScreen == NULL)
    {
        return 1;
    }


	//Step 1. Mute/Unmute Volume Settings
	if(IsVolumeMute())
        MuteVolume();
    else
        UnMuteVolume((int)g_CurrSysSettings.Volume);            

	//Step 2. Retrieveing Speech Packages
	if(!RetrieveSpeechPackages())
	{
		::MessageBox(NULL,L"No Voice Packages!",L"Error!!",MB_OK);
	}

	//Step 3. Start Speech Box
	StartSpeechBox();

	//Step 4. Issue Welcome Voice
#if !DISABLE_WELCOME_VOICE
	IssueWarnings(WELCOME);
#endif

	//Step 5. Opening User Database
	if(g_dmDatabase->OpenUserDB() != SQLITE_OK)
		pSplashScreen->DisplayErrorMsgBoxAndExit(0, _T(" \n Error: Can not open User Database! \n "));

	//Step 6. Displaying Map Brand Logo
#if !DISABLE_MAP_LOGO
	CQComponent *pMapLogoComp = pSplashScreen->GetChildComponent(GID_IMAGE_CURRENTMAPLOGO);
	if(pMapLogoComp!=NULL)
	{
		std::string sCurrentMapPath;
		int iCode = g_dmDatabase->GetValueFromUserDbSysInfo(USERDB_SYSINFO_CURRENTMAP,sCurrentMapPath);

        char sCurrentMapLogoPath[255];
		sprintf(sCurrentMapLogoPath, "%s\\%s\\Logo\\%s", g_szAppPath,sCurrentMapPath.c_str(),QID_MAP_LOGO_PATH);
		int nImageIndex = CQResource::SaveImageEntry(sCurrentMapLogoPath);

		CQResource::InitLoadImage();
		int iIndex = CQResource::LoadRawImage(nImageIndex);
		CQResource::ExitLoadImage();

		if(iIndex >= 0)
		{
			pMapLogoComp->SetBackgroundImage(iIndex);

			pMapLogoComp->SetVisible(TRUE);
		}
		else
		{
#if DISPLAY_DEFAULT_LOGO
			pMapLogoComp->SetVisible(TRUE);
#else
			pMapLogoComp->SetVisible(FALSE);
#endif
		}

		pSplashScreen->RedrawWindow();
	}
#endif

	//Step 7. Loading Images/POIs/Fonts
    eResource_Loading_Status eStatus = pSplashScreen->LoadImagesPOIFonts();

	switch(eStatus)
	{
	case eRESOURCE_LOADING_FILE_FAILED:
		pSplashScreen->DisplayErrorMsgBoxAndExit(90, _T(" \n Error: Configuration file loading failed! \n "));
		break;
	case eRESOURCE_LOADING_POI_FAILED:
		pSplashScreen->DisplayErrorMsgBoxAndExit(90, _T(" \n Error: POI loading failed! \n "));
		break;
	case eRESOURCE_LOADING_IMAGE_FAILED:
		pSplashScreen->DisplayErrorMsgBoxAndExit(90, _T(" \n Error: Image loading failed! \n "));
		break;
	case eRESOURCE_LOADING_FONT_FAILED:
		pSplashScreen->DisplayErrorMsgBoxAndExit(90, _T(" \n Error: Font loading failed! \n "));
		break;
	case eRESOURCE_LOADING_SUCCESS:
	default:
		break;
	}

	//Step Progress 20
    pSplashScreen->UpdateProgress(20);

	//Step 8. Reading Holux i6800 Device ID
#if DEVICE_ID_METHOD == 3
    char DevID[255];
    if (!GetDeviceID(DevID))
         return 0;
    if (strcmp(DevID,"4100540038"))
    {
        return 0;
    }
#endif
 
	//Step 9. Copy Map License
	pSplashScreen->CopyMapLicense();

    //Step 10. Open Map Database
    int check = g_dmDatabase->OpenDB();

	if(check == 0)
    {
		// Check the last known GPS location
        CFCoord oLastKnowCoord;
        CBBox oBBox;
        g_dmDatabase->GetMapBBox(oBBox);
        long minx = oBBox.GetTLCoord().X;
        long maxx = oBBox.GetBRCoord().X;
        long miny = oBBox.GetBRCoord().Y;
        long maxy = oBBox.GetTLCoord().Y;

        // retrieve all the map coordinate info to make sure coordinate system type is correct for coordinate conversion
        g_dmDatabase->GetLastKnownPosition(oLastKnowCoord);
        if( !(minx <= oLastKnowCoord.X && 
            maxx >= oLastKnowCoord.X &&
            maxy >= oLastKnowCoord.Y && 
            miny <= oLastKnowCoord.Y) )
        {
            oLastKnowCoord.X = (minx+maxx)/2.0f;
            oLastKnowCoord.Y = (miny+maxy)/2.0f;
            g_dmDatabase->SetLastKnownPosition(oLastKnowCoord);
        }

        g_oStateManager.m_iPolyOrientationFor3DBuilding = g_dmDatabase->GetPolyOrientationFor3DBuilding();
#if USE_UNIFIED_COST
        // get weightage speed
        g_dmDatabase->GetWeightageSpeed(gf_Weightage);
#endif
    }
    else
	{
		// cannot load DB pop up message and quit
        char sDevID[LEN_DEVICE_ID];
        bool l_bCheck = GetDeviceID(sDevID);

		std::string sMapPath;
        int iCode = g_dmDatabase->GetValueFromUserDbSysInfo(USERDB_SYSINFO_CURRENTMAP,sMapPath);

        CString sMap = CString(sMapPath.c_str());
		CString sTmp1,sTmp2;

		sTmp1 = CQResource::LoadResourceString(IDS_MAP_DB_LOAD_FAIL);

        CString sErrorMsg1 = L" " + sTmp1 + L" \n " + sMap;
        CString sErrorMsg2 = L" \n \n ";

        switch(check)
        {
        case DATASERVER_OPENDB_ERROR_CANNOTREADUSERDB:
            sErrorMsg2.Format(L" \n Error code = %d (_CANNOTREADUSERDB)\n ",check);
            break;
        case DATASERVER_OPENDB_ERROR_CANNOTREADLOCATIONDB:
            sErrorMsg2.Format(L" \n Error code = %d (_CANNOTREADLOCATIONDB)\n ",check);
            break;
        case DATASERVER_OPENDB_ERROR_CANNOTFINDANYFOLDERORFILE_NONE:
            sErrorMsg2.Format(L" \n Error code = %d \n(_CANNOTFINDANYFOLDERORFILE_NONE)\n ",check);
            break;
        case DATASERVER_OPENDB_ERROR_CANNOTFINDANYFOLDERORFILE_MAP:
            sErrorMsg2.Format(L" \n Error code = %d \n(_CANNOTFINDANYFOLDERORFILE_MAP)\n ",check);
            break;
        case DATASERVER_OPENDB_ERROR_CANNOTLOADANYMAP_NONE:
            sErrorMsg2.Format(L" \n Error code = %d \n(_CANNOTLOADANYMAP_NONE)\n ",check);
            break;
        case DATASERVER_OPENDB_ERROR_CANNOTLOADANYMAP_MAP:
            sErrorMsg2.Format(L" \n Error code = %d \n(_CANNOTLOADANYMAP_MAP)\n ",check);
            break;
        case DATASERVER_OPENDB_ERROR_MISMATCHEDMAPSCHEMA:
            sErrorMsg2.Format(L" \n Error code = %d \n(_MISMATCHEDMAPSCHEMA)\n ",check);
            break;
        case DATASERVER_OPENDB_ERROR_MAPIDNOTMATCHED:
            sErrorMsg2.Format(L" \n Error code = %d \n(_MAPIDNOTMATCHED)\n ",check);
            break;
        case DATASERVER_OPENDB_ERROR_CANNOTOPENMAPDB:
            sErrorMsg2.Format(L" \n Error code = %d \n(_CANNOTOPENMAPDB)\n ",check);
            break;
        case DATASERVER_OPENDB_ERROR_CANNOTOPENERPDB:
            sErrorMsg2.Format(L" \n Error code = %d (_CANNOTOPENERPDB)\n ",check);
            break;
        case DATASERVER_OPENDB_ERROR_CANNOTOPENROUTEDB:
            sErrorMsg2.Format(L" \n Error code = %d (_ERROR_CANNOTOPENROUTEDB)\n ",check);
            break;
        }

		sTmp1 = CQResource::LoadResourceString(IDS_PLS_CONTACT_VENDER);
		sTmp2 = CQResource::LoadResourceString(IDS_PLS_QUOTE_DEVICE_ID);

#if V8_POWERIQ
		CString sErrorMsg3 = sTmp1 + L" \n " + sTmp2 + L" \n " + CString(sDevID);
#elif V8_SALAGIS
        CString sErrorMsg3 = sTmp1 + L" \n (www.salagis.com)" + L" \n hotline: +94112366666 \n " + sTmp2 + L" \n " + CString(sDevID);
#else
		CString sErrorMsg3 = sTmp1 + L" \n (www.galactio.com)" + L" \n " + sTmp2 + L" \n " + CString(sDevID);
#endif
		CString sErrorMsg = sErrorMsg1+sErrorMsg2+sErrorMsg3;

		pSplashScreen->DisplayErrorMsgBoxAndExit(20, sErrorMsg);
    }

	//Step 11. Initialize Junction View
    CQResource::InitializeJunctionViewMemoryArea();

	//Step 13. Verify Software/TMC License
#if !DISABLE_LICENSING
	VerifyLicense();
#endif

	//Step 14. Initialize Routing Grid
	g_oNaviManager.Initialise(); //i2012_0717_005: consider routing grids to be loaded in a separate thread

	//Step Progress 40
	pSplashScreen->UpdateProgress(40);

	//Step 15. Start TMC Services
    StartTMCServices();

	//Step 16. Issue Satellite Locating
#if !DISABLE_SATELLITE_VOICE
    IssueWarnings(SAT_SEARCH);
#endif

	//Step Progress 50
    pSplashScreen->UpdateProgress(50);

	//Step 17. Start GPS Thread
    StartGPSThread();

	//Step Progress 70
    pSplashScreen->UpdateProgress(70);

	//Step 18. Battery status thread
#if DISPLAY_BATTERY_ICON
    startSystemInfoThread();
#endif

	//Step Progress 80
    pSplashScreen->UpdateProgress(80);

#if ENABLE_PREDICTED_PARKING
	InitParkingList();
#endif

	//Step Progress 85
    pSplashScreen->UpdateProgress(85);

	//Step 20. Start Dispatch Download Thread
#if ENABLE_ONLINE
    StartMapSynqAuthenticationThread();
#endif

#if ENABLE_DISPATCH_DEMO
	HANDLE hDispatch = CreateThread(NULL,0,thDownloadDispatch,NULL,0,NULL);
	QiSetThreadPriority(hDispatch,PRIORITY_MAINAPP_THREAD);
	CloseHandle(hDispatch);
#endif

	//Step 21. Start Traffic Ticker Thread
#if DISPLAY_TRAFFIC_TICKER
	HANDLE hTrafficTicker = CreateThread(NULL,0,thDownloadrafficTicker,NULL,0,NULL);
	QiSetThreadPriority(hTrafficTicker,PRIORITY_MAINAPP_THREAD);
	CloseHandle(hTrafficTicker);
#endif

	//Step 22. Preloading Windows
    CQResource::InitLoadImage();
    pSplashScreen->PreloadWindows();
    CQResource::ExitLoadImage();

	//Step Progress 90
    pSplashScreen->UpdateProgress(90);

	//Step 23. Getting User Name for Online Connected Feature
#if ENABLE_ONLINE
	wstring sUserName = L"";

    string sEncryptedPassword = "";
    string sDecryptPassword = "";
	g_dmDatabase->GetUserName(sUserName);
    g_dmDatabase->GetEncryptedPassword(sEncryptedPassword);
    
    decrypt(sEncryptedPassword,sDecryptPassword,rc4_dec);

    CQMyOnline::m_sLastSuccessUsername = CQMyOnline::m_sUsername = sUserName.c_str();
    CQMyOnline::m_sLastSuccessPassword = CQResource::to_widechar(sDecryptPassword.c_str(),sDecryptPassword.length()).c_str();
#endif

	//Step 24. Unload Map Brand Logo and Hide
#if !DISABLE_MAP_LOGO
	if(pMapLogoComp!=NULL)
	{
		CQResource::UnloadImage(pMapLogoComp->GetBackgroundImage());
		pMapLogoComp->SetVisible(FALSE);
	}
#endif

    g_oStateManager.StartNaviThread();

#if ENABLE_CDG_PROTOCOL
    HWND hWnd = FindWindow(_T(APPCLASS),_T(APPNAME));
    start_cdg_protocol(hWnd);
#endif

	CFCoord coord;
	// Update GPS Info
	if (g_CurrGPSData.ucGPSQuality)
	{
		//Current GPS Position
		coord = ConvertLongLatToMapCoord(g_CurrGPSData.Longitude, g_CurrGPSData.Latitude); //Current GPS position
	}
	else
	{
		//Get last known pos from database
		g_dmDatabase->GetLastKnownPosition(coord);
	}
	g_oMapManager.SetMapActivePointX(coord.X);
	g_oMapManager.SetMapActivePointY(coord.Y);

   	//Step 25. License Authenticating, Passcode Checking and Switching to Main Window
    pSplashScreen->SetCompleteLoadingFlag();
    pSplashScreen->SwitchToNextScreen();

    return 0;
}

/**----------------------------------------------------------------------------
* <strong> PURPOSE/PROCESS:</strong> This function is used to show/hide startup brand logo and map logo
*
* @param [in] void
* @return void
*
* <strong> AUTHOR:</strong> Aung
* 
* <strong> COMMENTS/HISTORY: </strong>
* 
* <strong> Notes from code: </strong>
------------------------------------------------------------------------------ */
void CQSplash::HandleStartupLogos()
{
    CQContainer *pContainer = (CQContainer *) GetComponent(QID_SPLASH_CONTAINER);
    CQComponent *pSplashLogo, *pMapsynqLogo;

    if(pContainer != NULL)	
    {
        pMapsynqLogo = pContainer->GetChildComponent(GID_IMAGE_MAPSYNQLOGO);

        bool bDisplayFullScreenDealer = false;

#if ENABLE_DEALER_FULL_SCREEN_STATUP
        bDisplayFullScreenDealer = g_bDealerStartupLogoAvailable;
#endif

        if(bDisplayFullScreenDealer)
        {
            if(pMapsynqLogo)
                pMapsynqLogo->SetVisible(FALSE);

            pSplashLogo = pContainer->GetChildComponent(QID_DEALER_FULL_SCREEN_STARTUP);
            if(pSplashLogo!=NULL)
                pSplashLogo->SetVisible(TRUE);
        }
        else
        {
            if(pMapsynqLogo)
                pMapsynqLogo->SetVisible(TRUE);


#if NO_TMC_SUPPORT
            pSplashLogo = pContainer->GetChildComponent(QID_SPLASHSCREEN_NON_TMC);
            if (pSplashLogo == NULL)
            {
                pSplashLogo = pContainer->GetChildComponent(QID_SPLASHSCREEN_TMC);
            }
            else
            {
                CQComponent *pTMCLogo = pContainer->GetChildComponent(QID_SPLASHSCREEN_TMC);
                if (pTMCLogo != NULL)
                {
                    pTMCLogo->SetVisible(FALSE);
                }
            }
#else
            pSplashLogo = pContainer->GetChildComponent(QID_SPLASHSCREEN_TMC);
#endif

            if(pSplashLogo!=NULL)
                pSplashLogo->SetVisible(TRUE);

            if (g_bDealerStartupLogoAvailable)
            {
                pSplashLogo = pContainer->GetChildComponent(QID_DEALER_SMALL_SCREEN_STARTUP);
                if(pSplashLogo!=NULL)
                    pSplashLogo->SetVisible(TRUE);
            }
        }

        pContainer->SetVisible(TRUE);
    }
}

/**----------------------------------------------------------------------------
* <strong> PURPOSE/PROCESS:</strong> This function is used to load all images/pois/fonts
*
* @param [in] void
* @return eResource_Loading_Status status of loading images/pois/fonts
*
* <strong> AUTHOR:</strong> Aung
* 
* <strong> COMMENTS/HISTORY: </strong>
* 
* <strong> Notes from code: </strong>
------------------------------------------------------------------------------ */
eResource_Loading_Status CQSplash::LoadImagesPOIFonts()
{
    std::string sPOIConfig = POI_IMAGE_RESOURCE_FILE_PATH;
    std::string sFontsConfig = FONT_RESOURCE_FILE_PATH;

	BOOL bStatus = FALSE; 

	//Load Images
    CQResource::InitLoadImage();
	bStatus = LoadImages(IMAGE_RESOURCE_FILE_PATH,g_szResourcePathForGenericBuild);
	CQResource::ExitLoadImage();

	if(!bStatus)
		return eRESOURCE_LOADING_IMAGE_FAILED;

	//Load POIs
	bStatus = LoadPOIs(sPOIConfig);
    if(!bStatus)
		return eRESOURCE_LOADING_POI_FAILED;
    
	//Load Fonts
	bStatus = LoadFonts(sFontsConfig);
	if(!bStatus)
		return eRESOURCE_LOADING_FONT_FAILED;

	return eRESOURCE_LOADING_SUCCESS;
}

/**----------------------------------------------------------------------------
* <strong> PURPOSE/PROCESS:</strong> This function is used to display Error Message and Exit the system in 30 seconds
*
* @param [in] p_iPercentage Percentage Interger for Progress Bar    
* @param [in] p_sErrorMsg Error Message String   
* @return void
*
* <strong> AUTHOR:</strong>  Aung
* 
* <strong> COMMENTS/HISTORY: </strong> Created common function to remove duplicated code
* 
* <strong> Notes from code: </strong>
------------------------------------------------------------------------------ */
void CQSplash::DisplayErrorMsgBoxAndExit(int p_iPercentage, const CString &p_sErrorMsg)
{
	int count=30;

	CString sTmp1 = CQResource::LoadResourceString(IDS_EXIT_AFTER);
	CString sTmp2 = CQResource::LoadResourceString(IDS_SEC);

	CString sErrorMsgTag;
	sErrorMsgTag.Format(L" \n %s %d %s ...",sTmp1,count,sTmp2);

	while(count>=0)
	{
		UpdateProgress(p_iPercentage,TRUE,p_sErrorMsg + sErrorMsgTag);
		Sleep(1000);
		sErrorMsgTag.Format(L" \n %s %d %s ...",sTmp1,count,sTmp2);
		count--;
	}

	exit(0);
}
