/*create table fpts3(id integer primary key , link_id integer);

/*create table fpts3(id integer primary key , link_id integer);

select addgeometrycolumn('fpts3', 'geometry', 4326, 'POINT', 2);

select GeometryType(geom) from merge_wgs;

select updategeometrysrid('merge_wgs','geom',4326)

insert into fpts3 select gid, objectid, St_startpoint(ST_LineMerge(geom::geometry)) from merge_wgs;

insert into fpts3 select gid+1000000, objectid, st_endpoint(St_linemerge(geom::geometry)) from merge_wgs;

select distinct on (a.id) a.id,min(st_astext(st_shortestline(a.geometry, b.geometry))),
min(st_distance(a.geometry, b.geometry)) as dist 
into results85 from fpts2 as a, fpts2 as b 
where not a.geometry = b.geometry and not a.link_id = b.link_id
and st_DWithin(a.geometry, b.geometry,0.00005) group by a.id ;

select ST_GeomFromText(min,4326) into qlayer85 from results85;*/
-----------------------------------------------------------------------------------------

--Using ST_Touches to check if the points can touch/lines can be drawn to touch the other points.

/*SELECT (ST_touches (merge_wgs.geom , b.geometry))
into results101 from fpts2 as b , merge_wgs as merge_wgs
where merge_wgs.geom = b.geometry --and 
and st_DWithin(merge_wgs.geom, b.geometry,0.00005);*/

--select ST_GeomFromText (min,4326) into qlayer100 from results100;

--------------------------------------------------------------------------------------

SELECT (ST_MakePoint (a.geometry , b.geometry)) into result102 from fpts2 as a,fpts2 as b;