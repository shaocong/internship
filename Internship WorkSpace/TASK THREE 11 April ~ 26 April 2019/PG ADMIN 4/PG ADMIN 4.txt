ST_MakeLine page 133

ST_LongestLine page 327

ST_ShortestLine page 338

ST_LineToCurve page 392

ST_LocateBetween page 436

PDF FILE PAGE 18. 

SELECT a.id ,min(ST_AsText(ST_LongestLine(a.geometry, b.geometry ))) , 
min(st_distance(a.geometry, b.geometry)) as dist
into results29 from fpts1 as a, fpts1 as b 
where not a.geometry = b.geometry and not a.link_id = b.link_id
and ST_DWithin(a.geometry, b.geometry,0.0005) group by a.id ;
